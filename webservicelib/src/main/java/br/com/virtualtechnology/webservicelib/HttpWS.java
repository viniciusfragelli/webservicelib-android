package br.com.virtualtechnology.webservicelib;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vinic on 03/11/2017.
 */

public class HttpWS {

    public int TIMEOUT_CONNECT = 3000;
    public int TIMEOUT_READ = 3000;
    public int TIMEOUT_WRITE = 3000;
    public boolean DEBUG = false;

    private Map<String, String> getParametrosDeEstatistica() {
        Map<String,String> parametros_estatistica = new HashMap<String, String>();
        parametros_estatistica.put("versaosistemaoperacional", android.os.Build.VERSION.RELEASE);
        parametros_estatistica.put("device", android.os.Build.MODEL);
        parametros_estatistica.put("system", "Android");
        parametros_estatistica.put("versao", BuildConfig.VERSION_NAME);
        return parametros_estatistica;
    }

    public void setConnectTimeout(int timeout) {
        TIMEOUT_CONNECT = timeout;
    }

    public void setReadTimeout(int timeout) {
        TIMEOUT_READ = timeout;
    }

    public void setWriteTimeout(int timeout) {
        TIMEOUT_WRITE = timeout;
    }

    public String convertParametrosEmString(Map<String, String> parametros) throws UnsupportedEncodingException {
        if(parametros != null){
            parametros.putAll(getParametrosDeEstatistica());
        }else{
            parametros = new HashMap<>();
            parametros.putAll(getParametrosDeEstatistica());
        }

        String urlParametros = null;
        for (Map.Entry<String, String> chave : parametros.entrySet()) {
            if (chave.getValue() != null) {
                urlParametros = (urlParametros == null) ? "" : urlParametros + "&";
                urlParametros += chave.getKey() + "=" + URLEncoder.encode(chave.getValue(), "utf-8");
            }
        }
        return urlParametros;
    }

    public String get(String url) throws IOException {
        return get(url,null);
    }

    public String get(String url, Map<String, String> parametros) throws IOException {
        String urlStr = url;
        String queryString = convertParametrosEmString(parametros);
        if (!queryString.equals("")) {
            urlStr += "?" + queryString;
        }

        if (DEBUG) {
            Log.d("HttpWS", "URL: " + url);
        }

        HttpURLConnection c = null;
        String result = "";
        try {
            URL u = new URL(urlStr);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            //c.setRequestProperty("Content-length", "0");
            c.setRequestProperty("Content-Type","application/json");
            c.setRequestProperty("charset", "utf-8");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(TIMEOUT_CONNECT);
            c.setReadTimeout(TIMEOUT_READ);
            c.connect();
            int status = c.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    if (!sb.toString().equalsIgnoreCase("[null]") && !sb.toString().equalsIgnoreCase("[null]\n")) {
                        result = sb.toString();
                    }
            }
        } finally {
            if (c != null)c.disconnect();
        }
        return result;
    }

    private String convertJSONEmParametros(String json) throws JSONException {
        String parametros = "";
        if(json != null){
            if(!json.isEmpty()){
                parametros = json
                        //.replace(" ","")
                        .replace("\n","")
                        .replace("{","")
                        .replace("}","")
                        .trim()
                        .replace(": ","=")
                        .replace(",  ","&")
                        .replace("\"","")
                        .replace(" ","%20");
                if(DEBUG) System.out.println("Conversão parametros do json"+parametros);
                return parametros;
            }else{
                throw new JSONException("Formato invalido de json");
            }
        }else{
            throw new JSONException("Formato invalido de json");
        }
    }

    public String post(String url, String json) throws IOException, JSONException {
        return post(url,json,null);
    }

    public String post(String url, String json, Map<String, String> parametros) throws IOException, JSONException {
        String urlStr = url;

        if(parametros != null){
            urlStr = urlStr + "?" + convertParametrosEmString(parametros);
        }

        String result = "";
        HttpURLConnection c = null;
        try {
            URL u = new URL(urlStr);
            if(DEBUG) System.out.println("URL doPost = "+urlStr);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("POST");
            c.setInstanceFollowRedirects(false);
            //c.setRequestProperty("Content-Type","application/json");
            //c.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            //c.setRequestProperty("Accept", "application/json");
            //c.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            //c.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
            c.setRequestProperty("charset", "utf-8");
            c.setUseCaches(false);
            c.setDoInput(true);
            c.setDoOutput(true);
            c.setConnectTimeout(TIMEOUT_CONNECT);
            c.setReadTimeout(TIMEOUT_READ);

            DataOutputStream out = new DataOutputStream(c.getOutputStream());
            out.write(convertJSONEmParametros(json).getBytes());

            out.flush();
            out.close();
            c.connect();
            int status = c.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    if (!sb.toString().equalsIgnoreCase("[null]") && !sb.toString().equalsIgnoreCase("[null]\n")) {
                        result = sb.toString();
                    }
            }
        } finally {
            if (c != null)c.disconnect();
        }

        return result;
    }

}
